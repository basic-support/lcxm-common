package cn.xuqiudong.common.util.collections;

import java.io.Serializable;

/**
 * 描述:
 *      对象的唯一标识
 * @author Vic.xu
 * @since 2024-10-12 9:20
 */
public interface EntityIdentification {

    Serializable getId();
}
