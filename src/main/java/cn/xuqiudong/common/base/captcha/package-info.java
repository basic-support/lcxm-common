/**
 * 说明: 验证码配置类和生成器
 * <p>
 * 使用方式：
 *  1. 引入配置文件  @Import(KaptchaConfig.class)
 *  2. 注入 KaptchaGenerate， 使用其generateWithoutToken 方法生成验证码
 *</p>
 * @author Vic.xu
 * @since 2023/5/17/0017 09:52
 */
package cn.xuqiudong.common.base.captcha;