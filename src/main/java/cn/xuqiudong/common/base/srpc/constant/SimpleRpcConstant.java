package cn.xuqiudong.common.base.srpc.constant;

/**
 * 描述:
 *
 * @author Vic.xu
 * @since 2024-06-25 10:02
 */
public final class SimpleRpcConstant {

    public static final String SIMPLE_RPC_URL = "/simple/rpc";
}
