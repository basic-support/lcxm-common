package cn.xuqiudong.common.base.srpc.protocol;

import cn.xuqiudong.common.base.srpc.model.XqdRequest;
import cn.xuqiudong.common.base.srpc.model.XqdResponse;
import cn.xuqiudong.common.base.srpc.provider.XqdServiceHolder;
import cn.xuqiudong.common.base.srpc.serializer.XqdSerializer;
import cn.xuqiudong.common.base.srpc.serializer.hessian.Hessian2Serializer;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

/**
 * 描述:
 *    服务端处理器
 * @author Vic.xu
 * @since 2024-07-01 9:51
 */
public class HttpServerHandler {

    private static XqdSerializer serializer = new Hessian2Serializer();

    public static void  handle(ServletRequest req, ServletResponse res){
        XqdResponse response;
        try {
            byte[] bytes = HttpProtocol.toByteArray(req.getInputStream());
            XqdRequest request = serializer.deserialize(bytes, XqdRequest.class);
            // 处理数据
            Object data = XqdServiceHolder.invokeMethod(request);
            response = XqdResponse.success(data);
        } catch (Exception e) {
            response = XqdResponse.error(e.getMessage());
        }
        try {
            byte[] bytes = serializer.serialize(response);
            IOUtils.write(bytes, res.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
