package cn.xuqiudong.common.base.exception;

/**
 * 描述: 没有权限访问的异常
 * @author Vic.xu
 * @since 2022-03-01 9:10
 */
public class UnauthorizedException extends RuntimeException {

    private static final long serialVersionUID = 858073011229605351L;

    public UnauthorizedException() {
        super();
    }
}
