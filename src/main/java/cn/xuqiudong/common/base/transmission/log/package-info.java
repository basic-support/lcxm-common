/**
 *  第三方对接中的日志记录：上报前数据， 上报后数据
 * @author VIC.xu
 * @since 2022-08-17
 *
 */
package cn.xuqiudong.common.base.transmission.log;